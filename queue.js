let collection = [];

// Write the queue functions below.

// Print Queue elements
const print = () => collection

// Enqueue new element
const enqueue = (item) => {collection.push(item); return collection;}

// Dequeue new element
const dequeue = (item) => {collection.shift(item); return collection;}

// Get first element
const front = () => collection[0];

// Get queue size
const size = () => collection.length;

// Queue is not empty
const isEmpty = () => (collection.length == 0) ? true : false

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	isEmpty,
	size
};